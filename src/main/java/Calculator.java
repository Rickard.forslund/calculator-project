import java.lang.reflect.Array;
import java.util.ArrayList;

public class Calculator {

    private int numberArray[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};

    public Calculator() {
    }

    public int add(String a, String b) {
        String onlyNumbers = "[0-9]+";
        if (a.matches(onlyNumbers) && (b.matches(onlyNumbers))) {
            int A = Integer.parseInt(a.trim());
            int B = Integer.parseInt(b.trim());
            return A + B;
        } else {
            throw new NumberFormatException("You can not use letters where numbers are suspected");
        }
    }

    public int sub(String a, String b) {
        String onlyNumbers = "[0-9]+";
        if (a.matches(onlyNumbers) && (b.matches(onlyNumbers))) {
            int A = Integer.parseInt(a.trim());
            int B = Integer.parseInt(b.trim());
            return A - B;
        } else {
            throw new NumberFormatException("You can not use letters where numbers are suspected");
        }
    }

    public int mult(String a, String b) {
        String onlyNumbers = "[0-9]+";
        if (a.matches(onlyNumbers) && (b.matches(onlyNumbers))) {
            int A = Integer.parseInt(a.trim());
            int B = Integer.parseInt(b.trim());
            return A * B;
        } else {
            throw new NumberFormatException("You can not use letters where numbers are suspected");
        }
    }

    public int div(String a, String b) {
        String onlyNumbers = "[0-9]+";
        if (a.matches(onlyNumbers) && (b.matches(onlyNumbers))) {
            int A = Integer.parseInt(a.trim());
            int B = Integer.parseInt(b.trim());
            return A / B;
        } else {
            throw new NumberFormatException("You can not use letters where numbers are suspected");
        }
    }

    public int countMyArray() {
        return numberArray.length;
    }

    public int countTotalOfArray() {
        int total = 0;
        for (int a : numberArray
        ) {
            total = total + a;
        }
        return total;
    }


}
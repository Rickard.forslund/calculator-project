import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class CalculatorTest {

    private Calculator calculator;

    @BeforeEach
    void init() {
        calculator = new Calculator();
    }

    @AfterEach
    private void afterEachTest() {
        calculator = null;
    }

    @Order(1)
    @DisplayName("<Calculator testing addition..  a + b >")
    @Tag("<fastTest>")
    @Test
    void testAddition() {
        Assertions.assertDoesNotThrow(() ->
                Assertions.assertEquals(7, calculator.add("3", "4")));
    }


    @Order(1)
    @DisplayName("<Calculator testing subtraction..  a - b >")
    @Tag("<fastTest>")
    @Test
    void testSubtraction() {
        Assertions.assertDoesNotThrow(() ->
                Assertions.assertEquals(1, calculator.sub("4", "3")));
    }

    @Order(1)
    @DisplayName("<Calculator testing multiplication..  a * b >")
    @Tag("<fastTest>")
    @Test
    void testMultiplication() {
        Assertions.assertDoesNotThrow(() ->
                Assertions.assertEquals(12, calculator.mult("3", "4")));
    }

    @Order(1)
    @DisplayName("<Calculator testing divided..  a / b >")
    @Tag("<fastTest>")
    @Test
    void testDiv()  {
        Assertions.assertDoesNotThrow(() ->
                Assertions.assertEquals(2, calculator.div("6", "3")));
    }

    @Order(2)
    @DisplayName("<testNumberArrayLength >")
    @Test
    void countTheNumbersOfMyArray() {
        Assertions.assertDoesNotThrow(() ->
                Assertions.assertEquals(9, calculator.countMyArray()));
    }

    @Order(3)
    @DisplayName("<testTotalSumOfArray >")
    @Test
    void countTheSumOfArray() {
        Assertions.assertDoesNotThrow(() ->
                Assertions.assertEquals(45, calculator.countTotalOfArray()));
    }


// __________________________  Exception Tests   ___________________________ //


    @Order(4)
    @DisplayName("<Testing Exception on addition input>")
    @Tag("<Exception>")
    @Test
    void testingNumberFormatExceptionOnAddMethod() {
        Assertions.assertThrows(NumberFormatException.class,() ->
                Assertions.assertEquals(7, calculator.add("p", "4")));
    }

    @Order(4)
    @DisplayName("<Testing Exception on subtraction input>")
    @Tag("<Exception>")
    @Test
    void testingNumberFormatExceptionOnSubMethod() {
        Assertions.assertThrows(NumberFormatException.class,() ->
                Assertions.assertEquals(7, calculator.sub("w", "4")));
    }

    @Order(4)
    @DisplayName("<Testing Exception on multiplication input>")
    @Tag("<Exception>")
    @Test
    void testingNumberFormatExceptionOnMultMethod() {
        Assertions.assertThrows(NumberFormatException.class,() ->
                Assertions.assertEquals(7, calculator.mult("i", "4")));
    }

    @Order(4)
    @DisplayName("<Testing Exception on divided input>")
    @Tag("<Exception>")
    @Test
    void testingNumberFormatExceptionOnDivMethod() {
        Assertions.assertThrows(NumberFormatException.class,() ->
                Assertions.assertEquals(7, calculator.div("u", "4")));
    }


    @Order(5)
    @Tag("Exception")
    @DisplayName("testing  NumberFormatException")
    @ParameterizedTest
    @ValueSource(strings = {"a", " 1 ", " 2 ", "B", "!"})
    void testingParameterizedNumberFormatException(String input) {
        Assertions.assertThrows(NumberFormatException.class,() ->
                calculator.add(input,input));
    }



}

